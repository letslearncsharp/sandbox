#Sand Box Log
---

##2019.04.29

* things to download
    * [Git-SCM](https://git-scm.com/download/win)
    	* this will be used to keep up to date with the source code
    	  get example files and all that.
	* [Unity Hub](https://public-cdn.cloud.unity3d.com/hub/prod/UnityHubSetup.exe)
		* this is going to be used to test out the projects and learn C#
		  and make games.
	* [Visual Studio 2019](https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=Community&rel=16)

* Setting things up in BitBucket.

* Created a Unity Project called `SandboxA`
so in the repo now there's an `img/` directory, a SandBox_Log.md (this) and a SandboxA repo for the unity project.
* So this first day is basically just getting all the software setup and ready to go.
	* to test that everything works, then you'll want to use Git-SCM or the GitBash as detailed below to clone this first repo to your computer, somthing that will be able to run windows + unity + visual studio.
	* make sure that once the repo is cloned you can open `SandboxA` with Unity
	* when the project is open in Unity make sure you can open scripts:
		* ![Open This!](img/OpenThis.png)
	* The SimpleCameraController.cs that's seen above should open Visual Studio 2019

---
##Using Git!
* expect some lesson like stuff here...
* git is a simple command line app.
	* ![gitbash](img/gitbash.png)
	* look around for this you'll wan to pin this to your start menu.
	we'll use this a lot.
	* open and the first thing you'll want to do is figure out where you are
	* `pwd` will show you where you are.
```bash
alexa@BUGGY MINGW64 ~
$ pwd
/c/Users/alexa
```
### Commands
* The first line is who you are
* the `$` is your command prompt.
* `pwd` is a command you should type in followed by enter.
* `/c/Users/alexa` is a reply from the shell telling you
what directory you're in.
* next lets move somewhere recognizable.
`cd Documents` will move you into your windows Documents directory
	* `cd` means Change Directory
	* `Documents` is the directory you want to change to
* then we can enter the command:
	- `mkdir LetsLearnCSharp`
This makes a new directory.
* ![letsmakeadirectory.png](img/letsmakeadirectory.png)
* This should be in your Documents directory
* to move into that...
`cd LetsLearnCSharp`
* as you might expect `pwd` will show you where you are.
```bash
alexa@BUGGY MINGW64 ~/Documents/LetsLearnCSharp
$ pwd
/c/Users/alexa/Documents/LetsLearnCSharp
```
### Git Clone
* now we can clone the sand box into LetsLearnCSharp.
```bash
git clone git@bitbucket.org:letslearncsharp/sandbox.git
```
* `git` is the software that will run the command
* `clone` which will ask for what to clone
* `git@bitbucket.org:letslearncsharp/sandbox.git` is a URL
	* it just lacks the http:// stuff you might expect
	* but it's an internet link!
* you'll see a bunch of text infering counting objects and writing compressing things
etc... it's doing it's thing.
* _yay!_ that' cloned the git repo!





